package controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import play.Logger;
import play.libs.Json;
import play.libs.ws.*;
import play.mvc.*;
import views.html.*;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */

public class HomeController extends Controller {

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
	
	private WSClient ws;
	
	@Inject
	HomeController(WSClient ws) 
	{
	       this.ws = ws;
	}
	String url = "https://demo.piwik.org/?module=API&method=API.getBulkRequest&format=json&urls[0]=method%3dVisitsSummary.get%26idSite%3d3%26date%3d2012-03-06%26period%3dday&urls[1]=method%3dVisitorInterest.getNumberOfVisitsPerVisitDuration%26idSite%3d3%26date%3d2012-03-06%26period%3dday&urls[2]=method%3dVisitFrequency.get%26idSite%3d3%26date%3d2012-03-06%26period%3dday&urls[3]=method%3dVisitTime.getVisitInformationPerServerTime%26idSite%3d3%26date%3d2012-03-06%26period%3dday";
	
    public Result index() 
    {
        return ok(index.render("Your new application is ready."));
    }
    
    public Result testWebService()
    {
    	return ok(processGetRequest(url));
    }
    
    
    private JsonNode processGetRequest(String url)
    {
    	JsonNode json = null;

		try
		{
			WSRequest request = ws.url("https://demo.piwik.org")
					.setQueryParameter("module", "API")
					.setQueryParameter("method", "API.getBulkRequest")
					.setQueryParameter("format", "json")
					.setQueryParameter("urls[0]", "method%3dVisitsSummary.get%26idSite%3d3%26date%3d2012-03-06%26period%3dday")
					.setQueryParameter("urls[1]", "method%3dVisitorInterest.getNumberOfVisitsPerVisitDuration%26idSite%3d3%26date%3d2012-03-06%26period%3dday")
					.setQueryParameter("urls[2]", "method%3dVisitFrequency.get%26idSite%3d3%26date%3d2012-03-06%26period%3dday")
					.setQueryParameter("urls[3]", "method%3dVisitTime.getVisitInformationPerServerTime%26idSite%3d3%26date%3d2012-03-06%26period%3dday");
			CompletionStage<WSResponse> responsePromise = request.get();
			CompletionStage<JsonNode> jsonPromise = responsePromise.thenApply(response -> response.asJson());
			json = jsonPromise.toCompletableFuture().get();
			Logger.info("Response JSON : " + json);
		}
		catch(Exception ex)
		{
			Logger.error("An Exceptation Occured During Get Request : " + ex.getMessage());
		}
		
		return json;
    }
    
    public Result getDashboard()
    {
    	JsonNode json = processGetRequest("");
    	JsonNode timeSpendVisitorsJson = json.get(1);
    	JsonNode visitorSummaryJson = json.get(3);
    	JsonNode uniqueVisitors = json.get(0);
    	JsonNode uniqueVisitor;
    	JsonNode timeSpendVisitor;
    	String label = "";
    	String lable2 = "";
    	String uniqueVisitorCount = "";
    	List<Long> timeSpent = new ArrayList<Long>();
    	String timeSpentData = "";
    	int total = 0;
    	
    	for(int i=0; i<timeSpendVisitorsJson.size(); i++)
    	{
    		timeSpendVisitor = timeSpendVisitorsJson.get(i);
    		int time = timeSpendVisitor.get("nb_visits").asInt();

    		if(time > 0)
    		{
    			total = total + time;
    			lable2 = lable2 + "'" + timeSpendVisitor.get("label").asText() + "', "; 
    			timeSpent.add(timeSpendVisitor.get("nb_visits").asLong());
    		}
    	}
    	
    	for(Long time : timeSpent)
    	{
    		timeSpentData = timeSpentData + Math.round((time.doubleValue()/total)*100) + ", ";
    	}
    	
    	for(int i=0; i<uniqueVisitors.size()/2; i++)
    	{
    		uniqueVisitor = uniqueVisitors.get(i);
    		
    		if(i == (uniqueVisitors.size()/2-1))
    		{
        		label = label + uniqueVisitor.get("label").asText();
        		uniqueVisitorCount = uniqueVisitorCount + uniqueVisitor.get("nb_uniq_visitors").asText();
    		}
    		else
    		{
        		label = label + uniqueVisitor.get("label").asText() + ", ";
        		uniqueVisitorCount = uniqueVisitorCount + uniqueVisitor.get("nb_uniq_visitors").asText() + ", ";
    		}
    	}
    	
    	List<String> visitorSummaryInfo = new ArrayList<String>();
    	visitorSummaryInfo.add("Number of Unique Visitors : " + visitorSummaryJson.get("nb_uniq_visitors").asText());
    	visitorSummaryInfo.add("Number of Visits : " + visitorSummaryJson.get("nb_visits").asText());
    	visitorSummaryInfo.add("Number of Users : " + visitorSummaryJson.get("nb_users").asText());
    	visitorSummaryInfo.add("Number of Actions Perfromed : " + visitorSummaryJson.get("nb_actions").asText());
    	visitorSummaryInfo.add("Maximum Actions Perfromed : " + visitorSummaryJson.get("max_actions").asText());
    	visitorSummaryInfo.add("Bounce Count : " + visitorSummaryJson.get("bounce_count").asText());
    	visitorSummaryInfo.add("Bounce Rate : " + visitorSummaryJson.get("bounce_rate").asText());
    	visitorSummaryInfo.add("Number of Actions per visit : " + visitorSummaryJson.get("nb_actions_per_visit").asText());
    	visitorSummaryInfo.add("Average Time on Site : " + visitorSummaryJson.get("avg_time_on_site").asText() + "s");
    	visitorSummaryInfo.add("Total Visit Lenght : " + visitorSummaryJson.get("sum_visit_length").asText() + "s");
    	
    	return ok("hi");
    	//return ok(views.html.dashtemplate.render(visitorSummaryInfo, label, uniqueVisitorCount, lable2, timeSpentData));
    }
    
    @SuppressWarnings("deprecation")
	public Result getDashboardByDate(String date)
    {
    	String impression = "";
    	String uniqueImpression = "";
    	String click = "";
    	String uniqueClick = "";
    	String year = "2017";
    	String month = "03";
    	String day = date.substring(6);
    	String dateString = "March "+ day + "th, 2017";
    	
    	
    	JsonNode visitorSummaryJson = getVisitorSummaryInformation(date);
    	JsonNode uniqueVisitors = getVisitorHoursInformation(date);
    	JsonNode timeSpendVisitorsJson = getVisitorTimeSpentInformation(date);
    	JsonNode clikInformation = getAdvertInfo(date);
    	
    	JsonNode clickInfo;
    	JsonNode uniqueVisitor;
    	JsonNode timeSpendVisitor;
    	String label = "";
    	String lable2 = "";
    	String uniqueVisitorCount = "";
    	List<Long> timeSpent = new ArrayList<Long>();
    	String timeSpentData = "";
    	int total = 0;
    	
    	for(int i=0; i<clikInformation.size(); i++)
    	{
    		clickInfo = clikInformation.get(i);
    		
    		if(clickInfo == null)
    		{
    			clickInfo = clikInformation;
    		}
    		
    		impression = impression + clickInfo.get("imp").asInt() +", ";
    		uniqueImpression = uniqueImpression + clickInfo.get("ump").asInt()+", ";
    		click = click + clickInfo.get("clk").asInt() +", ";
    		uniqueClick = uniqueClick + clickInfo.get("uclk").asText() +", ";
    	}
    	
    	for(int i=0; i<timeSpendVisitorsJson.size(); i++)
    	{
    		timeSpendVisitor = timeSpendVisitorsJson.get(i);
    		int time = timeSpendVisitor.get("nb_visits").asInt();

    		if(time > 0)
    		{
    			total = total + time;
    			lable2 = lable2 + "'" + timeSpendVisitor.get("label").asText() + "', "; 
    			timeSpent.add(timeSpendVisitor.get("nb_visits").asLong());
    		}
    	}
    	
    	for(Long time : timeSpent)
    	{
    		timeSpentData = timeSpentData + Math.round((time.doubleValue()/total)*100) + ", ";
    	}
    	
    	for(int i=0; i<uniqueVisitors.size()/2; i++)
    	{
    		uniqueVisitor = uniqueVisitors.get(i);
    		
    		if(i == (uniqueVisitors.size()/2-1))
    		{
        		label = label + uniqueVisitor.get("label").asText();
        		uniqueVisitorCount = uniqueVisitorCount + uniqueVisitor.get("nb_uniq_visitors").asText();
    		}
    		else
    		{
        		label = label + uniqueVisitor.get("label").asText() + ", ";
        		uniqueVisitorCount = uniqueVisitorCount + uniqueVisitor.get("nb_uniq_visitors").asText() + ", ";
    		}
    	}
    	
    	List<String> visitorSummaryInfo = new ArrayList<String>();
    	visitorSummaryInfo.add("Number of Unique Visitors : " + visitorSummaryJson.get("nb_uniq_visitors").asText());
    	visitorSummaryInfo.add("Number of Visits : " + visitorSummaryJson.get("nb_visits").asText());
    	visitorSummaryInfo.add("Number of Users : " + visitorSummaryJson.get("nb_users").asText());
    	visitorSummaryInfo.add("Number of Actions Perfromed : " + visitorSummaryJson.get("nb_actions").asText());
    	visitorSummaryInfo.add("Maximum Actions Perfromed : " + visitorSummaryJson.get("max_actions").asText());
    	visitorSummaryInfo.add("Bounce Count : " + visitorSummaryJson.get("bounce_count").asText());
    	visitorSummaryInfo.add("Bounce Rate : " + visitorSummaryJson.get("bounce_rate").asText());
    	visitorSummaryInfo.add("Number of Actions per visit : " + visitorSummaryJson.get("nb_actions_per_visit").asText());
    	visitorSummaryInfo.add("Average Time on Site : " + visitorSummaryJson.get("avg_time_on_site").asText() + "s");
    	visitorSummaryInfo.add("Total Visit Lenght : " + visitorSummaryJson.get("sum_visit_length").asText() + "s");
    	
    	
    	return ok(views.html.dashtemplate.render(visitorSummaryInfo, label, uniqueVisitorCount, lable2, timeSpentData, impression, uniqueImpression, click, uniqueClick, dateString));
    	
    }
    
	private JsonNode processPostRequest(String url, JsonNode jsonNode)
	{
		
		JsonNode json = null;
		
		try
		{
			CompletionStage<WSResponse> responsePromise = ws.url(url).post(jsonNode);
			CompletionStage<JsonNode> jsonPromise = responsePromise.thenApply(response -> response.asJson());
			json = jsonPromise.toCompletableFuture().get();
		}
		catch(Exception ex)
		{
			Logger.error("An Exceptation Occured During Post Request : " + ex.getMessage());
		}
		
		return json;
	}
	
	private JsonNode getAdvertInfo(String id)
	{
		JsonNode queryJson = getQueryJson(id);    	
    	JsonNode json = processPostRequest("http://localhost:9200/advertisment/clickinfo/_search",queryJson);
    	JsonNode outerHits = json.get("hits");
    	JsonNode innderHits = outerHits.get("hits").get(0);
    	JsonNode _source = innderHits.get("_source");
    	JsonNode data;
    	try
    	{
    		data = _source.get("data");
    		
    		if(data == null)
    		{
    			data = _source;
    		}
    	}
    	catch(Exception ex)
    	{
    		data = _source;
    		System.out.println("Exception Occured : " + ex.getMessage());
    		
    	}

    	return data;
	}
	
	private JsonNode getVisitorSummaryInformation(String id)
	{
		JsonNode queryJson = getQueryJson(id);    	
    	JsonNode json = processPostRequest("http://localhost:9200/piwik/visitorsummary/_search",queryJson);
    	JsonNode outerHits = json.get("hits");
    	JsonNode innderHits = outerHits.get("hits").get(0);
    	JsonNode _source = innderHits.get("_source");
    	JsonNode data = _source.get("data");
    	String dataString = data.toString();
    	dataString = dataString.replace("\\\"", "\"");
    	dataString = dataString.replace("]\"", "");
    	dataString = dataString.replace("\"[", "");
    	JsonNode formattedData = Json.parse(dataString);
    	return formattedData;
	}
	
	private JsonNode getVisitorHoursInformation(String id)
	{
		JsonNode queryJson = getQueryJson(id);    	
    	JsonNode json = processPostRequest("http://localhost:9200/piwik/visitorhourly/_search",queryJson);
    	JsonNode outerHits = json.get("hits");
    	JsonNode innderHits = outerHits.get("hits").get(0);
    	JsonNode _source = innderHits.get("_source");
    	JsonNode data = _source.get("data");
    	String dataString = data.toString();
    	dataString = dataString.replace("\\\"", "\"");
    	dataString = dataString.replace("]\"", "");
    	dataString = dataString.replace("\"[", "");
    	JsonNode formattedData = Json.parse(dataString);
    	return formattedData;
	}
	
	private JsonNode getVisitorTimeSpentInformation(String id)
	{
		JsonNode queryJson = getQueryJson(id);    	
    	JsonNode json = processPostRequest("http://localhost:9200/piwik/visitortimespent/_search",queryJson);
    	JsonNode outerHits = json.get("hits");
    	JsonNode innderHits = outerHits.get("hits").get(0);
    	JsonNode _source = innderHits.get("_source");
    	JsonNode data = _source.get("data");
    	String dataString = data.toString();
    	dataString = dataString.replace("\\\"", "\"");
    	dataString = dataString.replace("]\"", "");
    	dataString = dataString.replace("\"[", "");
    	JsonNode formattedData = Json.parse(dataString);
    	return formattedData;
	}
	
	private JsonNode getQueryJson(String id)
	{
		List<String> valueList = new ArrayList<String>();
		valueList.add(id);
		
		Map<String, List<String>> values = new HashMap<String, List<String>>();
		values.put("values", valueList);
		
		Map<String, Map<String, List<String>>> ids = new HashMap<String, Map<String, List<String>>>();
		ids.put("ids", values);
		
		Map<String, Map<String, Map<String, List<String>>>> query = new HashMap<String, Map<String, Map<String, List<String>>>>();
		query.put("query", ids);
		
		return Json.toJson(query);
	}
}
